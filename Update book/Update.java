import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Update {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");

	      // variables
	      final String url = "jdbc:mysql:///test";
	      final String user = "root";
	      final String password = "Root@123";

	      // establish the connection
	      Connection con = DriverManager.getConnection(url, user, password);
	      // create JDBC statement object
	      Statement st = con.createStatement();
	      
	      //How to update a record
	      Statement stmt = null;
	      stmt=con.createStatement();
	      String query="update bms set bookname='bcf' where bookid=8";
	      stmt.executeUpdate(query);
	      System.out.println("data is updated");
	}
}