import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Search {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");

	      // variables
	      final String url = "jdbc:mysql:///test";
	      final String user = "root";
	      final String password = "Root@123";

	      // establish the connection
	      Connection con = DriverManager.getConnection(url, user, password);
	      // create JDBC statement object
	      Statement st = con.createStatement();
	      
	      //
	      Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,0,0);
	      String query = "select * from bms";
	      ResultSet rs =  stmt.executeQuery(query);
	      rs.absolute(4);
	      
	      System.out.println("The row we need to search." );
	      System.out.println("bookid:"+rs.getInt(1)+" " );
	      System.out.println("bookname:"+rs.getString(2)+" " );
	      System.out.println("authorname:"+rs.getString(3)+" " );
	      System.out.println("edition:"+rs.getInt(4)+" " );
	    
	      rs.close();
	      
	}
	      
}
