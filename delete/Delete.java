import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Delete {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");

	      // variables
	      final String url = "jdbc:mysql:///test";
	      final String user = "root";
	      final String password = "Root@123";

	      // establish the connection
	      Connection con = DriverManager.getConnection(url, user, password);
	      // create JDBC statement object
	      Statement st = con.createStatement();
	      
	      
	      //How to delete a row
	      Statement stmt = null;
	      stmt=con.createStatement();
	      String query="delete from bms where bookid=15";
	      stmt.executeUpdate(query);
	      
	      System.out.println("data is deleted");

	}

}
